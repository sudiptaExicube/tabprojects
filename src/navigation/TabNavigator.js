import React from 'react';

//Bottom Navigation import
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();

//Screen Import
import Courier from '../screens/CourierPage/Courier';
import Mart from '../screens/MartPage/Mart';

export default function TabNavigator() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={Courier} />
      <Tab.Screen name="Settings" component={Mart} />
    </Tab.Navigator>
  );
}
